# -*- coding: utf-8 -*-
import os
import xlrd
import json

from django import http
from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.views.generic import FormView, TemplateView
from django import forms
from pytils.translit import translify
from django.views.decorators.csrf import csrf_exempt
try:
    from PIL import Image
except ImportError:
    import Image
import md5
import datetime
from django.contrib.auth.decorators import login_required
from apps.utils.utils import crop_image
from django.db.models import get_model 

from apps.products.models import Product, ProductVariant, Card

def handle_uploaded_file(f, filename, folder):
    name, ext = os.path.splitext(translify(filename).replace(' ', '_'))
    hashed_name=md5.md5(name+datetime.datetime.now().strftime("%Y%m%d%H%M%S")).hexdigest()
    path_name = settings.MEDIA_ROOT + '/uploads/' + folder + hashed_name + ext
    destination = open(path_name, 'wb+')

    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    return '/media/uploads/'+ folder + hashed_name + ext

@csrf_exempt
def upload_img(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'images/')

            #Resizing
            size = 650, 650
            im = Image.open(settings.ROOT_PATH + url)
            imageSize=im.size
            if (imageSize[0] > size[0]) or  (imageSize[1] > size[1]):
                im.thumbnail(size, Image.ANTIALIAS)
                im.save(settings.ROOT_PATH + url, "JPEG", quality = 100)

            response_data = { 'filelink': url }
            return http.HttpResponse(json.dumps(response_data), mimetype="text/html")

        else:
            return http.HttpResponse('error')
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')

@csrf_exempt
def upload_file(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'files/')
            url = '{"filelink":"%s","filename":"%s"}' % (url, request.FILES['file'].name)
            return http.HttpResponse(url)
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')

@login_required()
@csrf_exempt
def crop_image_view(request, app_name, model_name, id):
    model = get_model(app_name, model_name)
    output_size = model.crop_size
    if request.method != "POST":
        try:
            image = model.objects.get(pk=id).image
            return direct_to_template(request, 'admin/crop_image.html', locals())
        except model.DoesNotExist:
            raise http.Http404('Object not found')
    else:
        original_img = model.objects.get(pk=id)
        crop_image(request.POST, original_img, output_size)

    next = request.path.replace('crop/', '')
    return http.HttpResponseRedirect(next)


class LoadFileForm(forms.Form):
    file = forms.FileField(label="Файл")

class LoadProductsFromExcelView(FormView):
    template_name = 'admin/load_products.html'
    form_class = LoadFileForm
    success_url = '/load_success/'

    def form_valid(self, form):
        for variant in ProductVariant.objects.all():
            variant.is_published = False
            variant.save()

        input_excel = self.request.FILES['file']
        workbook = xlrd.open_workbook(file_contents=input_excel.read(), encoding_override="cp1251")
        sheet = workbook.sheet_by_index(0)

        for rownum in range(sheet.nrows):
            if rownum == 0:
                continue
            row = sheet.row_values(rownum)

            try:
                existing_variant = ProductVariant.objects.get(sku=row[0])
            except ProductVariant.DoesNotExist:
                existing_variant = None

            if existing_variant:
                existing_variant.price = row[2]
                existing_variant.is_published = True
                existing_variant.save()
            else:
                hint = u'Название из экселя: %s\nГруппа из экселя: %s\nЦена: %s\nSKU: %s'% (row[1], row[3], row[2], row[0])
                product = Product(title=row[1], group=row[3], hint=hint)
                product.save()
                variant = ProductVariant(product=product, price=row[2], sku=row[0])
                variant.save()

        return http.HttpResponseRedirect(self.get_success_url())



class LoadCardsFromExcelView(FormView):
    template_name = 'admin/load_cards.html'
    form_class = LoadFileForm
    success_url = '/load_success/'

    def form_valid(self, form):
        Card.objects.all().delete()
        input_excel = self.request.FILES['file']
        workbook = xlrd.open_workbook(file_contents=input_excel.read(), encoding_override="cp1251")
        sheet = workbook.sheet_by_index(0)

        for rownum in range(sheet.nrows):
            if rownum == 0:
                continue
            row = sheet.row_values(rownum)

            sk = int(row[0])

            try:
                existing_card = Card.objects.get(sk=sk)
            except:
                existing_card = None

            if existing_card:
                existing_card.sk = sk
                existing_card.save()
            else:
                card = Card(sk=sk, sum=row[1])
                card.save()

        return http.HttpResponseRedirect(self.get_success_url())


class LoadSuccessView(TemplateView):   
    template_name = 'admin/load_success.html'






