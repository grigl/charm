# -*- coding: utf-8 -*-

from fabric.api import *

env.hosts = ['']
env.passwords = {'':''}
project_repository = ''
project_name = ''
mysql_password = ''

def make_server():
    """
    Настройка сервера
    """
    if not local('apt-get update && apt-get upgrade').failed:
        local('apt-get -y install mysql-server mysql-client python-software-properties python-dev python-pip python-setuptools python-mysqldb libxml2-dev sendmail nano wget libjpeg62 libjpeg62-dev zlib1g-dev libfreetype6 libfreetype6-dev')
        local('add-apt-repository ppa:cherokee-webserver && apt-get update && apt-get -y install cherokee libcherokee-mod-mysql libcherokee-mod-libssl libcherokee-mod-rrd libcherokee-mod-geoip')
        local('wget http://octoberweb.ru/static/fabric/Imaging-1.1.7.tar.gz && tar xvfz Imaging-1.1.7.tar.gz && cd Imaging-1.1.7 && python install setup.py && cd .. && rm -R Imaging-1.1.7')
        local('pip install uwsgi==0.9.8.6 django==1.4.3 django-admin-tools sorl-thumbnail pytils django-mptt django-pagination django-simple-captcha pymorphy xhtml2pdf')
        local('wget http://octoberweb.ru/static/fabric/pymorphy.dict.ru.tar.gz && tar xvfz pymorphy.dict.ru.tar.gz && mkdir /usr/share/pymorphy && cp -R ru /usr/share/pymorphy && rm -R ru')

'''
def depoy():
    Скачивание и разворачивание сайта
    with settings(warn_only=True):
        #здесь командная строка с адресом архива, скачать распаковать в /home/user/
        #создать БД
        #скачать для wsgi и поправить его
        #local('/etc/init.d/cherokee restart && cherokee-admin -b')
        #prompt('Press <Enter> to continue or <Ctrl+C> to cancel.')  


def test-depoy():
    Скачивание и разворачивание сайта на тестовый сервак
    with settings(warn_only=True):
'''

# эти команды запускать на локалке
# def first_deploy():
# 	with settings():
# 		run('cd /home/user')
# 		run('git clone %s' % project_repository)
# 		run("mysql -u'root' -p'wrle2la5m' -e 'create database %s character set utf8;'" % project_name)

def first_deploy():
	# вставить проверку запушенности всех изменений в гит. И ПРАВА НА КАРТИНКИ БЛЕАТЬ
	with cd('/home/user/'):
		run('git clone %s' % project_repository)

	with cd('/home/user/%s' %project_name):
		# файлы проекта
		run('wget http://octoberweb.ru/static/fabric/django_wsgi.py')

		run('touch uwsgi.xml')
		run("echo '<uwsgi>' >> uwsgi.xml")
		run("echo '    <pythonpath>/home/user/%s</pythonpath>' >> uwsgi.xml" % project_name)
		run("echo '    <pythonpath>/home/user</pythonpath>' >> uwsgi.xml")
		run("echo '    <app mountpoint=\042/\042>' >> uwsgi.xml")
		run("echo '        <script>django_wsgi</script>' >> uwsgi.xml")
		run("echo '    </app>' >> uwsgi.xml")
		run("echo '    <touch-reload>/home/user/%s/reload.txt</touch-reload>' >> uwsgi.xml" % project_name)
		run("echo '</uwsgi>' >> uwsgi.xml")

		run('touch reload.txt')

		run('touch config/dbpass.py')
		run("echo 'DATABASE_PASSWORD = \042%s\042' >> config/dbpass.py" % mysql_password)

		# бд
		run("mysql -u'root' -p'%s' -e 'create database %s character set utf8;'" % (mysql_password, project_name))
		run("python manage.py syncdb")
		run("python manage.py migrate")

def deploy():
	with cd('/home/user/%s' % project_name):
		run('git fetch --all')
		run('git reset --hard origin/master')
		run("python manage.py migrate --ignore-ghost-migrations")

		run('touch reload.txt')

def cherokee_admin():
	run('cherokee-admin -u -b')

def test():
	with cd('/home/user/'):
		run('mkdir testt')

	with cd('/home/user/testt'):
		run('touch uwsgi.xml')
		run("echo '<uwsgi>' >> uwsgi.xml")
		run("echo '    <pythonpath>/home/user/%s</pythonpath>' >> uwsgi.xml" % project_name)
		run("echo '    <pythonpath>/home/user</pythonpath>' >> uwsgi.xml")
		run("echo '    <app mountpoint=\042/\042>' >> uwsgi.xml")
		run("echo '        <script>django_wsgi</script>' >> uwsgi.xml")
		run("echo '    </app>' >> uwsgi.xml")
		run("echo '    <touch-reload>/home/user/%s/reload.txt</touch-reload>' >> uwsgi.xml" % project_name)
		run("echo '</uwsgi>' >> uwsgi.xml")

