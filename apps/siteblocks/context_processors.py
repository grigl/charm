# -*- coding: utf-8 -*-
from apps.siteblocks.models import Settings
from apps.products.models import Category
from settings import SITE_NAME

def settings(request):
    try:
        contacts = Settings.objects.get(name='contacts_block').value
    except Settings.DoesNotExist:
        contacts = False

    in_ip = "IP Address for debug-toolbar: " + request.META['REMOTE_ADDR']

    try:
    	root_categories = Category.objects.root_nodes().filter(is_published=True)
    except:
    	root_categories = []

    try:
        cert_text = Settings.objects.get(name='cert_text').value
    except:
        cert_text = None

    try:
        phone_full = Settings.objects.get(name='phone').value.split(') ')

        phone_code = phone_full[0] + u')'
        phone = phone_full[1]
    except:
        phone_code = None
        phone = None

    try:
        card_bottom_text = Settings.objects.get(name='card_bottom_text').value
    except:
        card_bottom_text = None


    return {
        'contacts': contacts,
        'site_name': SITE_NAME,
        'root_categories': root_categories,
        'cert_text': cert_text,
        'phone_code': phone_code,
        'phone': phone,
        'card_bottom_text': card_bottom_text,
        'in_ip': in_ip,
    }