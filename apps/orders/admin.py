# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.orders.models import  CartProduct,Order,OrderProduct

class CartProductInlines(admin.TabularInline):
    model = CartProduct
    readonly_fields = ('product',)
    extra = 0

class CartAdmin(admin.ModelAdmin):
    list_display = ('id','create_date','sessionid',)
    list_display_links = ('id','create_date','sessionid',)
    list_filter = ('create_date',)
    inlines = [CartProductInlines,]

class OrderProductInlines(admin.TabularInline):
    model = OrderProduct
    readonly_fields = ('product' ,'count','sku' ,'product_price',)
    extra = 0

    def sku(self, obj):
        return obj.product.sku

class OrderAdmin(admin.ModelAdmin):
    list_display = ('id','create_date','full_name','phone', 'order_status', 'admin_summary',)
    list_display_links = ('id','full_name','create_date',)
    search_fields = ('full_name','email','phone','address','note','total_price',)
    list_filter = ('create_date','order_status',)
    readonly_fields = ('create_date','total_price')
    exclude = ['coupon_code', 'is_coupon_code','address','note',]
    inlines = [OrderProductInlines]

#admin.site.register(Cart, CartAdmin)
admin.site.register(Order, OrderAdmin)

