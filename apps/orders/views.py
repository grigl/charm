# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.core.mail.message import EmailMessage
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.generic import FormView, TemplateView, View
from django.views.generic.detail import DetailView
from django.template import RequestContext
from apps.orders.models import Cart, CartProduct, OrderProduct, Order
from apps.orders.forms import RegistrationOrderForm
from apps.products.models import ProductVariant
from apps.siteblocks.models import Settings
from pytils.numeral import choose_plural
import settings

from mailers import send_new_order_email


class ViewCart(FormView):
    form_class = RegistrationOrderForm
    template_name = 'orders/cart_detail.html'
    success_url = '/order_success/'

    def get_cart(self, **kwargs):
        cookies = self.request.COOKIES

        cookies_cart_id = False
        
        if 'charm_cart_id' in cookies:
            cookies_cart_id = cookies['charm_cart_id']

        session_id = self.request.session.session_key

        try:
            if cookies_cart_id:
                cart = Cart.objects.get(id=cookies_cart_id)
            else:
                cart = Cart.objects.get(sessionid=sessionid)
        except Cart.DoesNotExist:
            cart = False

        return cart

    def get_context_data(self, **kwargs):
        context = super(ViewCart, self).get_context_data()
        order_help_text = Settings.objects.get(name="order_help_text").value

        context['order_help_text'] = order_help_text
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        cart = self.get_cart()

        if cart:
            cart_products = cart.get_products_all()
        else:
            cart_products = False

        cart_str_total = u''
        is_empty = True
        count = 0
        
        if cart_products:
            is_empty = False
            cart_str_total = cart.get_str_total()
            count = cart.get_products_count()

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        context['order_form'] = form

        context['is_empty'] = is_empty
        context['cart_products'] = cart_products
        context['cart_str_total'] = cart_str_total
        context['count'] = count

        return render_to_response(self.template_name, context, context_instance=RequestContext(self.request))

    def post(self, request, *args, **kwargs):
        response = HttpResponse() 
        context = self.get_context_data(**kwargs)
        cart = self.get_cart()

        if cart:
            cart_products = cart.get_products_all()
        else:
            cart_products = False

        cart_str_total = u''
        is_empty = True
        count = 0

        if cart_products:
            is_empty = False
            cart_str_total = cart.get_str_total()
            cart_products_count = cart.get_products_count()

        data = request.POST.copy()
        order_form = RegistrationOrderForm(data)

        if order_form.is_valid():
            new_order = order_form.save()
            new_order.total_price = cart.get_total()
            new_order.save()

            for cart_product in cart_products:
                ord_prod = OrderProduct(
                    order=new_order,
                    count=cart_product.count,
                    product=cart_product.product,
                    product_price=cart_product.product.price
                )
                ord_prod.save()

            cart.delete() #Очистка и удаление корзины
            response.delete_cookie('charm_cart_id')

            send_new_order_email(new_order)

            return HttpResponseRedirect(self.success_url)
        else:
            context['order_form'] = order_form

            context['is_empty'] = is_empty
            context['cart_products'] = cart_products
            context['cart_str_total'] = cart_str_total
            context['count'] = count

            return render_to_response(self.template_name, context, context_instance=RequestContext(self.request))

view_cart = ViewCart.as_view()

# AJAX

class AddProductToCartView(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseRedirect('/')
        else:
            if 'product_id' not in request.POST:
                return HttpResponseBadRequest()

            try:
                product = ProductVariant.objects.get(id=int(request.POST['product_id']))
            except:
                return HttpResponseBadRequest()  

            try: 
                qty = int(request.POST['qty'])
                if qty < 1:
                    qty = 0
            except:
                qty = 0  

            cookies = request.COOKIES
            response = HttpResponse()

            cookies_cart_id = False
            if 'charm_cart_id' in cookies:
                cookies_cart_id = cookies['charm_cart_id']

            sessionid = request.session.session_key

            if not sessionid:
                Bop()

            if cookies_cart_id:
                try:
                    cart = Cart.objects.get(id=cookies_cart_id)
                except Cart.DoesNotExist:
                    cart = Cart.objects.create(sessionid=sessionid)
                response.set_cookie('charm_cart_id', cart.id, 1209600)
            else:
                try:
                    cart = Cart.objects.get(sessionid=sessionid)
                except Cart.DoesNotExist:
                    cart = Cart.objects.create(sessionid=sessionid)
                response.set_cookie('charm_cart_id', cart.id, 1209600)
            try:
                cart_product = CartProduct.objects.get(
                    cart=cart,
                    product=product,
                )
                if cart_product.is_deleted:
                    cart_product.is_deleted = False
                else:
                    cart_product.count += qty
                cart_product.save()
            except CartProduct.DoesNotExist:
                CartProduct.objects.create(
                    cart=cart,
                    product=product,
                )

            is_empty = True
            cart_products_count = cart.get_products_count()
            cart_total = cart.get_str_total()
            if cart_products_count:
                is_empty = False

            cart_html = render_to_string(
                'orders/block_cart.html',
                    {
                    'is_empty': is_empty,
                    'cart_products_count': cart_products_count,
                    'cart_total': cart_total,
                }
            )
            response.content = cart_html
            return response

add_product_to_cart = csrf_exempt(AddProductToCartView.as_view())

class DeleteProductFromCart(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseRedirect('/')
        else:
            if 'cart_product_id' not in request.POST:
                return HttpResponseBadRequest()

            try:
                cart_product = CartProduct.objects.get(id=int(request.POST['cart_product_id']))
            except CartProduct.DoesNotExist:
                return HttpResponseBadRequest()

            cart_product.is_deleted = True
            cart_product.save()
            cart = cart_product.cart

            response = HttpResponse()

            cart_str_total = cart.get_str_total()
            response.content = cart_str_total
            return response

delete_product_from_cart = csrf_exempt(DeleteProductFromCart.as_view())

class RestoreProductToCart(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseRedirect('/')
        else:
            if 'cart_product_id' not in request.POST:
                return HttpResponseBadRequest()
            else:
                cart_product_id = request.POST['cart_product_id']
                try:
                    cart_product_id = int(cart_product_id)
                except ValueError:
                    return HttpResponseBadRequest()

            try:
                cart_product = CartProduct.objects.get(id=cart_product_id)
            except CartProduct.DoesNotExist:
                return HttpResponseBadRequest()

            cart_product.is_deleted = False
            cart_product.save()

            response = HttpResponse()

            cart_products_count = cart_product.cart.get_products_count()
            cart_total = u''
            if cart_products_count:
                cart_total = cart_product.cart.get_str_total()
            data = u'''{"cart_total":'%s'}''' % cart_total
            response.content = data
            return response

restore_product_to_cart = csrf_exempt(RestoreProductToCart.as_view())

class ChangeCartCountView(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseRedirect('/')
        else:
            if 'cart_product_id' not in request.POST or 'new_count' not in request.POST:
                return HttpResponseBadRequest()
            try:
                new_count = int(request.POST['new_count'])
            except ValueError:
                return HttpResponseBadRequest()

            try:
                cart_product = CartProduct.objects.get(id=int(request.POST['cart_product_id']))
            except CartProduct.DoesNotExist:
                return HttpResponseBadRequest()

            cart_product.count = new_count
            cart_product.save()
            cart_str_total = cart_product.cart.get_str_total()
            return HttpResponse(cart_str_total)

change_cart_product_count = csrf_exempt(ChangeCartCountView.as_view())

