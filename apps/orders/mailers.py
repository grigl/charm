# -*- coding: utf-8 -*-
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from apps.siteblocks.models import Settings
from settings import DEFAULT_FROM_EMAIL as from_email


def send_new_order_email(new_order):
    subject = u'Шарм - Информация по заказу'
    subject = u''.join(subject.splitlines())
    message = render_to_string(
        'orders/message_template.html',
            {
            'order': new_order,
            'products': new_order.get_products()
        }
    )

    try:
        emailto = Settings.objects.get(name='main_email').value
    except Settings.DoesNotExist:
        emailto = False

    if emailto:
        msg = EmailMessage(subject, message, from_email, [emailto])
        msg.content_subtype = "html"
        msg.send()