# -*- coding: utf-8 -*-
from django import forms
from apps.orders.models import Order

class RegistrationOrderForm(forms.ModelForm):
    full_name = forms.CharField(error_messages={'required': 'Введите ФИО'}, widget=forms.TextInput(attrs={}))
    phone = forms.CharField(error_messages={'required': 'Введите ваш номер телефона'}, widget=forms.TextInput(attrs={}))

    class Meta:
        model = Order
        exclude = ('create_date','total_price','order_status')

    def clean_phone(self):
        data = self.cleaned_data.get('phone', '')

        if data:
            data = u'+7 ' + data

        return data