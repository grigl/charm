# -*- coding: utf-8 -*-
from django import forms
from django.views.generic import TemplateView, ListView
from products.models import Product, Category, Banner


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView,self).get_context_data(**kwargs)

        banners = Banner.objects.published()

        context['is_index_page'] = 'True'
        context['banners'] = banners
        return context

index = IndexView.as_view()