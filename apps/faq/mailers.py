# -*- coding: utf-8 -*-
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from apps.siteblocks.models import Settings
from settings import DEFAULT_FROM_EMAIL as from_email


def send_new_question_email(saved_object):
    subject = u'Шарм - Новый вопрос'
    subject = u''.join(subject.splitlines())
    message = render_to_string(
        'faq/message_template.html',
            {
            'saved_object': saved_object
        }
    )

    try:
        emailto = Settings.objects.get(name='main_email').value
    except Settings.DoesNotExist:
        emailto = False

    if emailto and saved_object.email:
        msg = EmailMessage(subject, message, from_email, [emailto])
        msg.content_subtype = "html"
        msg.send()