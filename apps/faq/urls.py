# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from views import questions_list, fag_form


urlpatterns = patterns('',
    url(r'^$', questions_list, name='faq_url'),
    url(r'^form/$', fag_form, name='faq_form_url'),
    #url(r'^sendquestion/$','apps.faq.views.question_form'),
    #url(r'^checkform/$','apps.faq.views.save_question_form'),
)