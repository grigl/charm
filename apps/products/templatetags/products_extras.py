# -*- coding: utf-8 -*-
import random

from django import template
from apps.products.models import Certificate, Product

register = template.Library()

@register.inclusion_tag('products/_get_footer_product.html')
def get_footer_product():
	try:
		product = Product.objects.filter(show_in_footer=True).order_by('?')[0]
	except: 
		product = None

	return {
		'product': product
	}


@register.inclusion_tag('products/_card_and_certificate.html')
def get_card_and_certificate(cart=True, certificate=True, get_all=False):
	cert_set = Certificate.objects.all()

	with_cart = cart
	with_certificate = certificate

	if get_all == False:
		choice = random.randint(0, 1)
		
		if choice == 0:
			with_cart = False
		elif choice == 1:
			with_certificate = False


	if len(cert_set) == 0:
		cert_str_price = None
	elif len(cert_set) == 1:
		cert_str_price = str(cert_set[0].value) + u' рублей'
	else:
		cert_str_price = u'от ' + str(min([x.value for x in cert_set])) + u' до ' + str(max([x.value for x in cert_set])) + u' рублей'

	return {
		'cert_str_price': cert_str_price,
		'with_cart': with_cart,
		'with_certificate': with_certificate
	}


@register.inclusion_tag('products/_card_and_certificate.html')
def get_card():
	with_cart = True
	with_certificate = False

	return {
		'with_cart': with_cart,
		'with_certificate': with_certificate
	}


@register.inclusion_tag('products/_card_and_certificate.html')
def get_certificate():
	cert_set = Certificate.objects.all()

	with_cart = False
	with_certificate = True

	if len(cert_set) == 0:
		cert_str_price = None
	elif len(cert_set) == 1:
		cert_str_price = str(cert_set[0].value) + u' рублей'
	else:
		cert_str_price = u'от ' + str(min([x.value for x in cert_set])) + u' до ' + str(max([x.value for x in cert_set])) + u' рублей'

	return {
		'cert_str_price': cert_str_price,
		'with_cart': with_cart,
		'with_certificate': with_certificate
	}