# -*- coding: utf-8 -*-
import os,md5
from datetime import datetime, date, timedelta
from itertools import groupby
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, View


from models import Category, Product, Certificate, Card, MailingPhoneNumber, SpecialOffer

class ProductsSearch(ListView):
    context_object_name = 'products'
    template_name = 'products/product_search_list.html'
    model = Product

    def get_queryset(self):
        request = self.request

        if 'q' in request.GET:
            q = request.GET['q']
        else:
            q = None

        if q:            
            qs = super(ProductsSearch, self).get_queryset().filter(category__isnull = False).filter(
                Q(title__icontains=q)|
                Q(description__icontains=q)|
                Q(full_description__icontains=q)
            )
        else: 
            qs = None

        return qs

    def get_context_data(self, **kwargs):
        context = super(ProductsSearch, self).get_context_data(**kwargs)
        request = self.request

        product_list = self.get_queryset()

        if product_list:
            product_count = product_list.count()
            product_list = product_list.order_by('category')
        else: 
            product_count = 0

        if 'q' in request.GET:
            q = request.GET['q']
        else:
            q = None

        context['product_list'] = product_list
        context['product_count'] = product_count
        context['search_q'] = q

        return context

search_view = ProductsSearch.as_view()


class ProductsCategorySearch(ListView):
    context_object_name = 'products'
    template_name = 'products/product_category_search_list.html'
    model = Product

    def get_queryset(self):
        request = self.request
        kwargs = self.kwargs

        category_id = kwargs.get('category_id')
        category = Category.objects.get(pk=category_id)

        if 'q' in request.GET:
            q = request.GET['q']
        else:
            q = None

        if q:            
            qs = super(ProductsCategorySearch, self).get_queryset().filter(category=category).filter(
                Q(title__icontains=q)|
                Q(description__icontains=q)|
                Q(full_description__icontains=q)
            )
        else: 
            qs = None

        return qs

    def get_context_data(self, **kwargs):
        context = super(ProductsCategorySearch, self).get_context_data(**kwargs)
        request = self.request
        kwargs = self.kwargs

        product_list = self.get_queryset()

        category_id = kwargs.get('category_id')
        category = Category.objects.get(pk=category_id)

        if product_list:
            product_count = product_list.count()
        else: 
            product_count = 0

        if 'q' in request.GET:
            q = request.GET['q']
        else:
            q = None

        context['product_list'] = product_list
        context['product_count'] = product_count
        context['search_q'] = q
        context['category'] = category

        return context

category_search_view = ProductsCategorySearch.as_view()


class ProductDetail(DetailView):
    queryset = Product.objects.select_related()
    context_object_name = 'product'
    template_name = 'products/product_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetail,self).get_context_data()
        context['current_category'] = self.object.category
        return context

product_view = ProductDetail.as_view()

class CatalogAllView(ListView):
    template_name = 'products/catalog.html'
    model = Product
    paginate_by = 15

    def get_context_data(self, **kwargs):
        context = super(CatalogAllView,self).get_context_data(**kwargs)
        context['category_level'] = None
        return context


class CatalogView(ListView):
    template_name = 'products/catalog.html'
    model = Product
    paginate_by = 15

    def get_params(self):
        params = self.request.GET

        params_props = {}
        for prop_key in params.keys():
            if 'prop_' in prop_key:
                params_props[prop_key.replace('prop_', '')] = params.getlist(prop_key)
        if params_props == {}:
            params_props = None

        params_brands = params.getlist('brands')
        if params_brands == []:
            params_brands = None

        return params_props, params_brands

    def get_current_category(self):
        full_path = self.kwargs['page_full_path']
        current_category = Category.objects.get(full_path=full_path)
        return current_category

    def get_current_category_level(self):
        current_category = self.get_current_category()
        all_levels = sorted(set(map(lambda x: x.level, current_category.get_root().get_descendants(include_self=True))))
        current_category_level = all_levels[::-1][current_category.level]+1
        return current_category_level

    def get_queryset(self):
        current_category = self.get_current_category()
        params_props, params_brands = self.get_params()

        qs = current_category.product_set.all()

        if params_brands:
            qs = qs.filter(brand__in=params_brands)

        if params_props:
            # qs = qs.filter(prop_slug__in=params_props.keys())
            prod_ids = []
            for prop_name in params_props.keys():
                products = qs.filter(prop_slug=prop_name)

                for product in products:
                    filtered_variants_count = product.productvariant_set.filter(prop_value__in=params_props[prop_name]).count()
                    if filtered_variants_count > 0:
                        prod_ids.append(product.id)

            qs = qs.filter(pk__in=prod_ids)

        return qs

    def get_context_data(self, **kwargs):
        params_props, params_brands = self.get_params()

        context = super(CatalogView,self).get_context_data(**kwargs)
        context['current_category'] = self.get_current_category()
        context['category_level'] = self.get_current_category_level()
        context['params_props'] = params_props
        context['params_brands'] = params_brands
        return context



class CategoryModalView(TemplateView):
    template_name = 'products/category_modal.html'

    def get_current_category(self):
        full_path = self.kwargs['page_full_path']
        current_category = Category.objects.get(full_path=full_path)
        return current_category

    def get_context_data(self, **kwargs):
        context = super(CategoryModalView,self).get_context_data(**kwargs)

        current_category = self.get_current_category()
        context['current_category'] = current_category
        return context

    def get(self, request, **kwargs):
        if request.is_ajax():
            context = self.get_context_data()
            html_code = render_to_string(self.template_name, context)
            return HttpResponse(html_code)



class BrandsModalView(TemplateView):
    template_name = 'products/brands_modal.html' 

    def get_current_category(self):
        full_path = self.kwargs['page_full_path']
        current_category = Category.objects.get(full_path=full_path)
        return current_category

    def get_params(self):
        params = self.request.GET

        params_props = {}
        for prop_key in params.keys():
            if 'prop_' in prop_key:
                params_props[prop_key.replace('prop_', '')] = params.getlist(prop_key)
        if params_props == {}:
            params_props = None

        params_brands = params.getlist('brands')
        if params_brands == []:
            params_brands = None

        return params_props, params_brands

    def get_context_data(self, **kwargs):
        context = super(BrandsModalView,self).get_context_data(**kwargs)

        current_category = self.get_current_category()
        brands = current_category.get_brands_by_latter()

        params_props, params_brands = self.get_params()

        context['current_category'] = current_category
        context['brands'] = brands
        context['params_props'] = params_props
        context['params_brands'] = params_brands

        return context

    def get(self, request, **kwargs):
        if request.is_ajax():
            context = self.get_context_data()
            html_code = render_to_string(self.template_name, context)
            return HttpResponse(html_code)


class PropValuesModalView(TemplateView):
    template_name = 'products/prop_values_modal.html'

    def get_current_category(self):
        full_path = self.kwargs['page_full_path']
        current_category = Category.objects.get(full_path=full_path)
        return current_category

    def get_params(self):
        params = self.request.GET

        params_props = {}
        for prop_key in params.keys():
            if 'prop_' in prop_key:
                params_props[prop_key.replace('prop_', '')] = params.getlist(prop_key)
        if params_props == {}:
            params_props = None

        params_brands = params.getlist('brands')
        if params_brands == []:
            params_brands = None

        return params_props, params_brands

    def get_context_data(self, **kwargs):
        context = super(PropValuesModalView,self).get_context_data(**kwargs)

        current_category = self.get_current_category()
        current_prop_slug = self.kwargs['prop_slug']

        params_props, params_brands = self.get_params()

        props = current_category.get_properties_with_count()
        current_props = [x for x in props if x['prop_slug'] == current_prop_slug][0]

        context['current_category'] = current_category
        context['current_props'] = current_props
        context['params_props'] = params_props
        context['params_brands'] = params_brands
        return context

    def get(self, request, **kwargs):
        if request.is_ajax():
            context = self.get_context_data()
            html_code = render_to_string(self.template_name, context)
            return HttpResponse(html_code)



class CertificateView(TemplateView):
    template_name = 'products/certificate.html'

    def get_context_data(self, **kwargs):
        context = super(CertificateView, self).get_context_data()

        certificate_set = Certificate.objects.all()

        context['certificate_set'] = certificate_set
        return context

class CardView(TemplateView):
    template_name = 'products/card.html'

    def get_context_data(self, **kwargs):
        context = super(CardView, self).get_context_data()

        sk = self.request.GET.get('q')

        if sk:
            try:
                your_card = Card.objects.get(sk=sk)
            except:
                your_card = None
        else:
            your_card = None

        if sk and not your_card:
            show_error = True
        else: 
            show_error = False
        
        context['your_card'] = your_card
        context['show_error'] = show_error
        return context


class SpecialOffersListView(ListView):
    template_name = 'products/special_offers.html'
    model = SpecialOffer
    paginate_by = 12
    context_object_name = 'special_offers'
    queryset = model.objects.published()


class SpecialOfferView(DetailView):
    template_name = 'products/special_offer_details.html'
    model = SpecialOffer
    context_object_name = 'special_offer'
    queryset = model.objects.published()


class SMSView(TemplateView):
    def post(self, request):
        MailingPhoneNumber.objects.create(phone_number=request.POST.get('phone_number'))

        return HttpResponseRedirect(request.POST.get('return_to'))
