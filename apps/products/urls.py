# -*- coding: utf-8 -*-
from django.conf.urls.defaults import url, patterns, include
from views import *


urlpatterns = patterns('',
    url(r'^search/$',search_view, name='search_url'),
    url(r'^search/(?P<category_id>\d+)/$', category_search_view, name='category_search_url'),
    url(r'^products/(?P<pk>\d+)/$',product_view, name='products_detail'),

    # modals urls
    url(r'^catalog/(?P<page_full_path>[\w\d_/-]+)/get_category_modal/$', CategoryModalView.as_view()),
    url(r'^catalog/(?P<page_full_path>[\w\d_/-]+)/get_all_brands_modal/$', BrandsModalView.as_view()),
    url(r'^catalog/(?P<page_full_path>[\w\d_/-]+)/get_all_prop_values_modal/(?P<prop_slug>[\w\d_/-]+)/$', PropValuesModalView.as_view()),
    
    # catalog urls
    url(r'^catalog/all/$', CatalogAllView.as_view(), name='all_catalog_url'),
    url(r'^catalog/(?P<page_full_path>[\w\d_/-]+)/$', CatalogView.as_view(), name='catalog_url'),

    # certificate url
    url(r'^certificate/$', CertificateView.as_view(), name='certificate_url'),

    # card url
    url(r'^card/$', CardView.as_view(), name='card_url'),

    # special offers urls
    url(r'^special_offers/$', SpecialOffersListView.as_view(), name='special_offers_list_url'),
    url(r'^special_offer/(?P<slug>[\w\d_/-]+)/$', SpecialOfferView.as_view(), name='special_offer_url'),

    # sms mailing url
    url(r'^subscribe/$', SMSView.as_view(), name='subscribe_url'),
)
