# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe  
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor
from sorl.thumbnail.admin import AdminImageMixin
from mptt.admin import MPTTModelAdmin
from django.http import HttpResponse


from models import *

class CategoryAdmin(AdminImageMixin, MPTTModelAdmin):
    list_display = ('id','name','parent','alias','order','is_published',)
    list_display_links = ('id','name',)
    list_editable = ('order','is_published',)

admin.site.register(Category, CategoryAdmin)

class BrandAdmin(admin.ModelAdmin):
    list_display = ('id','name','alias','order','is_published',)
    list_display_links = ('id','name',)
    list_editable = ('order','is_published',)

admin.site.register(Brand, BrandAdmin)

class ProductVariantInline(admin.TabularInline):
    model = ProductVariant
    extra = 0
# class PhotoInline(admin.TabularInline):
#     model = Photo
#--Виджеты jquery Редактора
class ProductAdminForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea(attrs={'cols': 110, 'rows': 10}), required=False)
    description.label=u'Описание'

    full_description = forms.CharField(widget=Redactor(attrs={'cols': 110, 'rows': 40}), required=False)
    full_description.label=u'Полное описание'

    hint = forms.CharField(widget=forms.Textarea(attrs={'readonly': 'readonly'}))
    hint.label = u'Подсказка'

    class Meta:
        model = Product
#--Виджеты jquery Редактора
class ProductAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('id','title', 'category','brand', 'order',)
    list_display_links = ('id','title',)
    list_editable = ('order',)
    list_filter = ('group','category','brand',)
    search_fields = ('title', 'description', 'full_description','category__name')
    inlines = [ProductVariantInline]
    form = ProductAdminForm

    actions = ['join_products']

    # распихиваем все варианты всех товаров к первому, 
    # а остальные варианты и товары удаляем.
    # кроме того, запихиваем все данные в подсказку, на всякий.
    def join_products(self, request, queryset):
        master_product = queryset[0]
        new_hint = master_product.hint

        for product in queryset[1:]:
            new_hint += '\n'
            new_hint += product.hint
            for variant in product.productvariant_set.all():
                new_variant = ProductVariant.objects.create(
                              product=master_product,
                              sku=variant.sku,
                              price=variant.price,
                              old_price=variant.old_price,
                              prop_value=variant.prop_value
                              )
                new_variant.save()
                variant.delete()
            product.delete()

        master_product.hint = new_hint
        master_product.save()

    join_products.short_description = "Объединить товары"

admin.site.register(Product, ProductAdmin)

class BannerAdmin(admin.ModelAdmin):
    list_display = ('id','url','is_published',)
    list_editable = ('is_published',)

admin.site.register(Banner, BannerAdmin)

class CertificateAdmin(admin.ModelAdmin):
    list_display = ('value','is_published',)
    list_editable = ('is_published',)

admin.site.register(Certificate, CertificateAdmin)

class CardAdmin(admin.ModelAdmin):
    list_display = ('sk','sum','is_published',)
    list_editable = ('is_published',)
    search_fields = ('sk',)

admin.site.register(Card, CardAdmin)


class SpecialOfferForm(forms.ModelForm):
    text = forms.CharField(widget=Redactor(attrs={'cols': 110, 'rows': 40}))

    class Meta:
        model = SpecialOffer
class SpecialOfferAdmin(admin.ModelAdmin):
    list_display = ('title','is_published',)
    list_editable = ('is_published',)
    form = SpecialOfferForm

admin.site.register(SpecialOffer, SpecialOfferAdmin)


class MailingPhoneNumberAdmin(admin.ModelAdmin):
    list_display = ('phone_number',)

admin.site.register(MailingPhoneNumber, MailingPhoneNumberAdmin)
