# -*- coding: utf-8 -*-
import os, datetime
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User

from pytils.translit import translify
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField
from mptt.models import MPTTModel, TreeForeignKey, TreeManager

from apps.utils.managers import PublishedManager

def str_price(price):
    if not price:
        return u'0'
    value = u'%s' %price
    if price._isinteger():
        value = u'%s' %value[:len(value)-3]
        count = 3
    else:
        count = 6
    if len(value)>count:
        ends = value[len(value)-count:]
        starts = value[:len(value)-count]
        return u'%s %s' %(starts, ends)
    else:
        return value

def file_path_Category(instance, filename):
    return os.path.join('images','category',  translify(filename).replace(' ', '_') )
class Category(MPTTModel):
    parent = TreeForeignKey('self', verbose_name=u'Родительская категория', related_name='children', blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(verbose_name=u'Название', max_length=100)
    alias = models.CharField(verbose_name=u'Алиас', max_length=100)
    full_path = models.CharField(verbose_name=u'Полный путь', max_length=100, unique=True, null=True, blank=True)
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Category, null=True, blank=True)
    order = models.IntegerField(verbose_name=u'Порядок сортировки',default=10)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)

    objects = TreeManager()


    def __unicode__(self):
            return self.name

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'
        ordering = ['order',]


    class MPTTMeta:
            order_insertion_by = ['order']


    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        category_set = self.get_ancestors(include_self=True)
        if category_set.count() == 1:
            self.full_path = self.alias
        else:
            path = ''
            for i, category in enumerate(category_set):
                if i>0:
                    path += '/'
                path += category.alias

            self.full_path = path
        super(Category, self).save(*args, **kwargs)
        descendants_set = self.get_descendants(include_self=False)
        if descendants_set.count() > 0:
            for descendant in descendants_set:
                descendant.save()

        Category.objects.rebuild() #rebulding tree


    def is_published_category(self):
        if self.is_published == True:
            is_published = True
        else: 
            is_published = False

        return is_published

    def get_absolute_path(self):
        category_set = self.get_ancestors(include_self=True)
        path = '/'
        for category in category_set:
            path += category.alias
            path += '/'
        return path

    def get_descendants_count(self):
        return self.get_descendants().count()

    def is_empty(self):
        category_set = self.get_descendants(include_self=True)

        products = []
        for cat in category_set:
            for product in cat.product_set.all():
                products.append(product)

        if len(products) > 0:
            is_empty = False
        else: 
            is_empty = True
        return is_empty

    def is_root(self):
        if self.get_root() == self:
            is_root = True
        else:
            is_root = False

        return is_root

    def get_siblings_with_self(self):
        return self.get_siblings(include_self=True)

    def get_products(self):
        return self.product_set.filter(is_published=True)

    def get_variants(self):
        variants = ProductVariant.objects.filter(product__category=self).filter(is_published=True)
        return variants

    def get_products_count(self):
        return self.get_products().count()

    def get_properties(self):
        products = self.product_set.filter(is_published=True).prefetch_related('productvariant_set')
        properties = []

        for product in products:
            product_prop_hash = product.get_property_hash()
            product_prop_hash['prop_values'] = sorted(set(product_prop_hash['prop_values']))

            if properties == []:                
                properties.append(product_prop_hash)
            elif not product.prop_slug in map(lambda x: x['prop_slug'], properties):
                properties.append(product_prop_hash)
            else:
                for prop_value in product_prop_hash['prop_values']:
                    all_prop_values = sum(map(lambda x: x['prop_values'], properties),[])
                    if not prop_value in all_prop_values:
                        for prop_hash in properties:
                            if prop_hash['prop_name'] == product_prop_hash['prop_name']:
                                prop_hash['prop_values'].append(prop_value)

        for prop in properties:
            prop['prop_values'] = sorted(prop['prop_values'])

        return properties

    def get_properties_with_count(self):
        properties = self.get_properties()
        variants = self.get_variants()

        for prop in properties:
            new_prop_values = []
            for value in prop['prop_values']:
                value_hash = {}
                count = variants.filter(prop_value=value).count()
                value_hash['value'] = value
                value_hash['count'] = count
                new_prop_values.append(value_hash)
            
            prop['prop_values'] = new_prop_values
        
        return properties

    def get_brands(self):
        products = self.get_products()
        products_pks = products.values_list('brand', flat=True)
        brands = Brand.objects.filter(pk__in=list(products_pks))

        if brands.count() == 0:
            brands = None

        return brands

    def get_brands_with_count(self):
        brands = self.get_brands()
        brands_with_count = []

        for brand in brands:
            brand_hash = {}
            count = brand.product_set.filter(category=self).count()
            brand_hash['brand'] = brand
            brand_hash['count'] = count
            brands_with_count.append(brand_hash)

        return brands_with_count

    def get_brands_by_latter(self, **kwargs):
        brands = self.get_brands()
        latters = []
        for brand in brands:
            latters.append(brand.name[0])

        brand_list = {}
        for latter in latters:
            brand_list[latter] = []
            for brand in brands:
                if brand.name[0] == latter: 
                    brand_list[latter].append(brand)
        return brand_list

    def has_brands_or_props(self):
        brands = self.get_brands()
        props = [ x for x in self.get_properties() if x['prop_name'] != '' ]

        if props != [] or brands != None:
            has_brands_or_props = True
        else: 
            has_brands_or_props = False

        return has_brands_or_props

    def has_subcategories(self):
        subcategory_set = self.get_descendants(include_self=False)

        if subcategory_set.count() > 0:
            has_subcategories = True
        else:
            has_subcategories = False

        return has_subcategories

    def has_not_empty_subcategories(self):
        subcategory_set = self.get_descendants(include_self=False)

        if len([x for x in subcategory_set if not x.is_empty()]) > 0:
            has_subcategories = True
        else:
            has_subcategories = False

        return has_subcategories

    def get_full_title(self):
        ancestors = self.get_ancestors(include_self=True)

        full_title = ' / '.join([ x.name for x in ancestors ])

        return full_title
        

class Brand(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=100)
    alias = models.CharField(verbose_name=u'Алиас', max_length=100, unique=True)
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Category)
    order = models.IntegerField(verbose_name=u'Порядок сортировки',default=10)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)

    objects = PublishedManager()

    def __unicode__(self):
            return self.name

    class Meta:
        verbose_name = u'Брэнд'
        verbose_name_plural = u'Брэнды'
        ordering = ['-order', 'name']

    def get_absolute_url(self):
        return u'' #self.alias

    def get_products(self):
        return self.product_set

    def get_products_count(self):
        return self.get_products().count()



def file_path_Product(instance, filename):
    return os.path.join('images','products',  translify(filename).replace(' ', '_') )
class Product(models.Model):
    category = TreeForeignKey(Category, verbose_name=u'Категория', null=True, blank=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Брэнд', null=True, blank=True)
    group = models.CharField(verbose_name=u'Группа в эксэле', max_length=200, null=True, blank=True)
    title = models.CharField(verbose_name=u'Название', max_length=400, null=True, blank=True)
    hint = models.TextField(blank=True, verbose_name=u'Подсказка')
    description = models.TextField(blank=True)
    full_description = models.TextField(blank=True)
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Product,blank=True)
    prop_name = models.CharField(verbose_name=u'Название свойства', max_length=200, null=True, blank=True)
    prop_slug = models.CharField(verbose_name=u'Техническое название свойства на латинице', max_length=200, null=True, blank=True)
    prop_label = models.CharField(verbose_name=u'Обозначение свойства', max_length=200, null=True, blank=True)
    show_in_footer = models.BooleanField(verbose_name=u'Показывать в футере', default=False)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)

    order = models.IntegerField(verbose_name=u'Порядок сортировки',default=10)

    objects = PublishedManager()

    # def save(self, *args, **kwargs):
    #     super(Product, self).save(*args, **kwargs)

    class Meta:
        verbose_name =_(u'product_item')
        verbose_name_plural =_(u'product_items')

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('products_detail',kwargs={'pk': '%s'%self.id})

    def get_price(self):
        price_list = []

        for variant in self.productvariant_set.all():
            price_list.append(variant.price)

        if len(price_list) == 1 or len(set(price_list)) == 1:
            price = u'<span class="big">' + str_price(price_list[0]) + u'</span> руб'
        else:
            price = u'от <span class="big">' + str_price(min(price_list)) + u'</span> до <span class="big">' + str_price(max(price_list)) + u'</span> руб'

        return price

    def get_property(self):
        prop_hash = self.get_property_hash()

        if len(set(prop_hash['prop_values'])) == 1:
            if not prop_hash['prop_values'][0] == 'None':
                prop = prop_hash['prop_values'][0] + u' ' + prop_hash['prop_label']
            else:
                prop = False
        else:
            prop_values = filter(None, prop_hash['prop_values'])

            if prop_values:
                prop_values_are_numbers = False
                for val in prop_values:
                    try:
                        float(val)
                        prop_values_are_numbers = True
                    except ValueError:
                        pass
                        

                if prop_values_are_numbers:
                    prop = u'от ' + min(prop_values) + u' до ' + max(prop_values) + u' ' + prop_hash['prop_label']
                else:
                    prop = u'например, ' + prop_values[0] + u' ' + prop_hash['prop_name']
            else:
                prop = ''

        return prop


    def get_property_hash(self):
        prop = {}
        prop['prop_name'] = self.prop_name
        prop['prop_label'] = self.prop_label
        prop['prop_slug'] = self.prop_slug

        prop_values_list = []
        for variant in self.productvariant_set.all():
            prop_values_list.append(variant.prop_value)

        prop['prop_values'] = prop_values_list

        return prop


    def get_str_price(self):
        return str_price(self.price)

    def get_str_old_price(self):
        return str_price(self.old_price)

    def get_photos(self):
        return self.photo_set.all()

    def get_avg_price(self):
        variant_set = self.productvariant_set.exclude(price=0)
        prices_list = [x.price for x in variant_set]
        avg_price = sum(prices_list)/len(prices_list)

        return avg_price


    def get_related(self):
        related_products = self.category.product_set.all().exclude(pk=self.id)[:3]

        return related_products


class ProductVariant(models.Model):
    product = models.ForeignKey(Product, verbose_name=u'Продукт')
    sku = models.IntegerField(verbose_name=u'sku', null=True, blank=True)
    prop_value = models.CharField(verbose_name=u'Значение свойства', blank=True, max_length=240)
    price = models.DecimalField(verbose_name=u'Цена', decimal_places=2, max_digits=10, blank=True, null=True)
    old_price = models.DecimalField(verbose_name=u'Старая цена', decimal_places=2, max_digits=10, blank=True, null=True)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)
    order = models.IntegerField(verbose_name = u'Порядок сортировки', default = 10, help_text = u'чем больше число, тем выше располагается элемент')

    objects = PublishedManager()

    class Meta:
        verbose_name = u'вариант'
        verbose_name_plural = u'варианты'
        ordering = ['-order']

    def __unicode__(self):
        return self.product.title + u' вариант ' + str(self.sku)

    def get_str_price(self):
        return str_price(self.price)

    def get_str_old_price(self):
        return str_price(self.old_price)

    def on_sale(self):
        if self.old_price and self.price:
            on_sale = True
        else: 
            on_sale = False

        return on_sale



def file_path_Product(instance, filename):
     return os.path.join('images','products',  translify(filename).replace(' ', '_') )
class Photo(models.Model):
    product = models.ForeignKey(Product, verbose_name=u'Товар')
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Product)

    class Meta:
        verbose_name =_(u'product_photo')
        verbose_name_plural =_(u'product_photos')

    def __unicode__(self):
        return u'Фото товара %s' %self.product.name



#Класс фраз для поиска
class Phrase(models.Model):
    example = models.CharField(verbose_name=u'Пример фразы', max_length=100)

    class Meta:
        verbose_name =_(u'phrase')
        verbose_name_plural =_(u'phrases')

    def __unicode__(self):
        return self.example



class Card(models.Model):
    sk = models.CharField(verbose_name=u'Номер', max_length=200, null=True, blank=True)
    sum = models.DecimalField(verbose_name=u'Сумма', decimal_places=2, max_digits=10, blank=True, null=True)
    is_published = models.BooleanField(verbose_name = u'Действительна', default=True)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'Карта'
        verbose_name_plural = u'Карты'
        ordering = ['sk']

    def __unicode__(self):
        return u'карта номер %s' % self.sk



def file_path_Certificate(instance, filename):
     return os.path.join('images','certificates', translify(filename).replace(' ', '_') )
class Certificate(models.Model):
    value = models.IntegerField(verbose_name=u'Номинал')
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Certificate,blank=True)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'Сертификат'
        verbose_name_plural = u'Сертификаты'
        ordering = ['value']

    def __unicode__(self):
        return u"Сертификат номиналом %s" % self.value



def file_path_Banner(instance, filename):
     return os.path.join('images','banners', translify(filename).replace(' ', '_') )
class Banner(models.Model):
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_Banner, null=True)
    url = models.URLField(verbose_name=u'ссылка', null=True, blank=True)
    is_published = models.BooleanField(verbose_name = u'Опубликовано', default=True)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'

    def __unicode__(self):
        return u"Баннер для %s" % self.url



def file_path_SpecialOffer(instance, filename):
     return os.path.join('images','specialoffers', translify(filename).replace(' ', '_') )
class SpecialOffer(models.Model):
    title = models.CharField(
        verbose_name = u'заголовок',
        max_length = 250,
    )
    slug = models.CharField(
        max_length=200, 
        verbose_name=u'Название в адресной строке', 
        unique=True,
        help_text=u'Название в адресной строке на латинице',
        null=True,
        blank=True,
    )
    image = ImageField(verbose_name=u'Картинка', upload_to=file_path_SpecialOffer, null=True, blank=True)
    text = models.TextField(
        verbose_name = u'текст',
    )
    is_published = models.BooleanField(
        verbose_name = u'опубликовано',
        default = True,
    )
    # timestamps
    created_at = models.DateTimeField(
        verbose_name = u'дата создания',
        default = datetime.datetime.now(),
        editable = False,
    )


    objects = PublishedManager()

    
    class Meta:
        ordering = ['-created_at', '-id',]
        verbose_name = u'Специальное предложение'
        verbose_name_plural = u'Специальные предложения'

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = translify(self.title).replace(' ', '_')
        super(SpecialOffer, self).save(*args, **kwargs)


class MailingPhoneNumber(models.Model):
    phone_number = models.CharField(
        verbose_name = u'номер телефона',
        max_length = 250,
    )

    class Meta:
        ordering = ['-id',]
        verbose_name = u'Номер телефона для смс рассылка'
        verbose_name_plural = u'Номера телефонов для смс рассылки'

    def __unicode__(self):
        return self.phone_number









