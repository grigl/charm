var Checkboxes = function(){
    $('body').on('click', 'li.checkbox-box', function(){
        el = $(this).find('span.checkbox');
        input = el.find('input');
        var checked = el.hasClass('checked');
        if (checked) {
            el.removeClass('checked');
            input.attr('checked', false);
        } else {
            el.addClass('checked');
            input.attr('checked', true);
        };
    });
};

var Fancybox = function(){
    $('.fancybox').each(function(){
        var el = $(this);

        el.fancybox({
            padding: '0px',
            minHeight: 'auto',
            tpl : {
                closeBtn : '<div class="modal-close"></div>'
            },
            helpers: {
                overlay : {
                    opacity: 0.5,
                    locked: false
                }
            },
            'ajax' : {
                type: "GET",
                data: $('#filters-form').serialize()
            }
        });
    });

    $('.fancybox-hint').each(function(){
        var el = $(this);

        el.fancybox({
            padding: '0px',
            minHeight: 'auto',
            tpl : {
                closeBtn : '<div class="modal-close"></div>'
            },
            helpers: {
                overlay : {
                    opacity: 0.5,
                    locked: false
                }
            }
        });
    });

    $('body').on('submit', '.fancybox-response', function(e){
        e.preventDefault();

        var data = $(this).serialize();
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function(data){
                $.fancybox({
                    padding: 0,
                    minHeight: 'auto',
                    tpl: {
                        closeBtn: '<div class="modal-close"></div>'
                    },
                    helpers: {
                        overlay: {
                            opacity: 0.5,
                            locked: false,
                        }
                    },
                    content: data
                });
            }
        });
    });
};


var Slider = function(){
    var slider = $('.banner-slider').bxSlider({
        auto: true,
        pause: 4500,
        mode: 'fade',
        pager: false,
        controls: false,
        onSlideBefore: function(newIndex){swichCurrentP(newIndex.attr('data-idx'))}
    });

    var swichCurrentP = function(idx){
        var elIdx = parseInt(idx);
        el = $('.banner-slider-controls li[data-idx="'+elIdx+'"]');

        var href = $('.banner-slider li[data-idx="'+elIdx+'"] a').attr('href');
        if (href == '') {
            $('.banner-link').removeAttr('href');
        } else {
            $('.banner-link').attr('href', href);
        };

        $('.banner-slider-controls li').removeClass('current');
        el.addClass('current');
    }
    $('.banner-slider').on('mouseover', function(){
        // alert('stop');
        slider.stopAuto();
        return false;
    });
    $('.banner-slider').on('mouseout', function(){
        slider.startAuto();
        // alert('start');
    });
    $('.banner-slider-controls li').on('click', function(e){
        e.preventDefault();
        var el = $(this);
        var slideIndex = parseInt(el.attr('data-idx'))-1;
        slider.goToSlide(slideIndex);
        slider.stopAuto();
    });
}

var Menu = function(){
    var menu = $('.main-menu');

    menu.on('hover', 'li.with-modal', function(){
        var el = $(this);
        var modalContainer = el.find('.modal-container');
        modalContainer.show();
    });

    menu.on('mouseleave', 'li.with-modal', function(){
        var el = $(this);
        var modalContainer = el.find('.modal-container');
        setTimeout(function() {
            var isHovered = el.is(":hover");
            if (!isHovered) {
                modalContainer.hide();
            };
        }, 200);
    });

    menu.on('click', 'li.with-modal a.main-menu-link', function(e){
        e.preventDefault();
        return false;
    });
};

var BuyForm = function(){
    $('.buy-form').each(function(){
        var form = $(this);
        var url = form.attr('action');
        var method = form.attr('method');

        form.on('submit', function(e){
            e.preventDefault();
            var data = form.serialize();
            
            if (data) {
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    success:function(data){
                        $('.cart-box').replaceWith(data);
                    },
                    error:function(jqXHR,textStatus,errorThrown){

                    }
                });
            };
        });
    });
}

var PriceInput = function() {
    $('.price-input-wrap').each(function(){
        var el = $(this);
        var input = el.find('.price-input');

        el.on('click', '.minus', function(){
            var amount = parseInt(input.attr('value'));
            input.attr('value', amount-1);
        });

        el.on('click', '.plus', function(){
            var amount = parseInt(input.attr('value'));
            input.attr('value', amount+1);
        });
    });
}

var Cart = function() {
    var RecalcCartTotal = function() {
        var prices = [];
        $.each($('.cart-product.not-deleted .total-price'), function(){
            prices.push(parseInt($(this).text()));
        });
        
        var totalCartPrice = prices.reduce(function(previousValue, currentValue){
            return currentValue + previousValue;
        }, 0);

        $('.cart-total-price').text(totalCartPrice);
    };

    $('.cart-qnt-box').each(function(){
        var el = $(this);
        var input = el.find('.price-input');
        var cartProduct = el.parents('.cart-product');
        var cartProductId = cartProduct.data('product-id');
        var price = parseInt(cartProduct.find('.description .price').text().split(' ').join(''));

        var ChangeAmount = function(newAmount, cartProductId) {
            $.ajax({
                type: 'post',
                url: '/change_cart_product_count/',
                data: {'new_count':newAmount,'cart_product_id':cartProductId},
                success:function(data){
                    RecalcTotal(newAmount);
                },
                error:function(jqXHR,textStatus,errorThrown){

                }
            });
        };
        
        var RecalcTotal = function(amount) {
            var totalEl = cartProduct.find('.price-value-box .total-price');
            totalPrice = amount*price;
            totalEl.html(totalPrice);

            RecalcCartTotal();
        };

        el.on('click', '.minus', function(){
            var amount = parseInt(input.attr('value'));
            var newAmount = amount-1;
            input.attr('value', newAmount);

            ChangeAmount(newAmount, cartProductId);
        });

        el.on('click', '.plus', function(){
            var amount = parseInt(input.attr('value'));
            var newAmount = amount+1;
            input.attr('value', newAmount);

            ChangeAmount(newAmount, cartProductId);
        });
    });

    $('.delete-cart-product').each(function(){
        var el = $(this);
        var cartProduct = el.parents('.cart-product');
        var cartProductId = cartProduct.data('product-id');

        el.on('click', function(e){
            e.preventDefault();

            $.ajax({
                type: 'post',
                url: '/delete_product_from_cart/',
                data: {'cart_product_id':cartProductId},
                success:function(data){
                    cartProduct.removeClass('not-deleted');
                    cartProduct.addClass('deleted');
                    cartProduct.find('.restore-btn-box').show();

                    RecalcCartTotal();
                },
                error:function(jqXHR,textStatus,errorThrown){

                }
            });
        });
    });

    $('.restore-cart-product').each(function(){
        var el = $(this);
        var cartProduct = el.parents('.cart-product');
        var cartProductId = cartProduct.data('product-id');

        el.on('click', function(e){
            e.preventDefault();

            $.ajax({
                type: 'post',
                url: '/restore_product_to_cart/',
                data: {'cart_product_id':cartProductId},
                success:function(data){
                    el.parents('.restore-btn-box').hide();
                    cartProduct.removeClass('deleted');
                    cartProduct.addClass('not-deleted');

                    RecalcCartTotal();
                },
                error:function(jqXHR,textStatus,errorThrown){

                }
            });
        });
    });
};

$(function(){
    Checkboxes();
    Fancybox();
    Slider();
    Menu();
    BuyForm();
    PriceInput();
    Cart();
});